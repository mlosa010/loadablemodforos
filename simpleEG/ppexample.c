#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

// i am neglecting to put info macros because this is a simple example
MODULE_LICENSE("GPL");
MODULE_AUTHOR("MANNY");
MODULE_DESCRIPTION("simple module");
MODULE_VERSION("0.1");

static char *name ="world";// default argument if user does not specify in terminal
module_param(name, charp, S_IRUGO);
MODULE_PARM_DESC(name,"the name to display in /var/log/kern.log");

//this does the stuff you want
static int __init hello_init(void){
  printk(KERN_INFO "TDR: hi %s\n", name);
  return 0;
}


// this closes the program
static void __exit hello_exit(void){
  printk(KERN_INFO "TDR:Goodbye %s from ubu\n", name);
}


module_exit(hello_exit);
module_init(hello_init);
